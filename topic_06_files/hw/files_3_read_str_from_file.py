"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""

import pickle


def read_str_from_file(my_path):

    my_tuple = (None, '44', True, ['qwerty', 55, 4 - 2], {1: 'asdfgh', 2: 78})

    with open(my_path, 'wb') as pickle_file:
        pickle.dump(my_tuple, pickle_file)

    with open(my_path, 'rb') as load_pickle_file:
        print(pickle.load(load_pickle_file))


if __name__ == '__main__':
    ex_path = 'my_tuple.pkl'
    read_str_from_file(ex_path)
