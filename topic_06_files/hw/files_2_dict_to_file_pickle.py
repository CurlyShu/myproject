"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(my_path, my_dict):
    with open(my_path, 'wb') as pickle_file:
        pickle.dump(my_dict, pickle_file)


if __name__ == '__main__':
    ex_dict = {1: 1, 2: ['sdfdf', None, 1, False], 3: (True, 'popopo')}
    path = 'my_dict.pkl'

    save_dict_to_file_pickle(path, ex_dict)

    with open(path, 'rb') as m_f:
        dict_loaded = pickle.load(m_f)

    print(f'type: {type(dict_loaded)}')     # "type: <class 'dict'>"
    print(f'equal: {ex_dict == dict_loaded}')   # equal: True
