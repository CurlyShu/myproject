from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:

    """
    Класс Farm.

    Поля:
    животные (list из произвольного количества Goat и Chicken): zoo_animals
    (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
    наименование фермы: name,
    имя владельца фермы: owner.

    Методы:
    get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
    get_chicken_count: вернуть количество куриц на ферме,
    get_animals_count: вернуть количество животных на ферме,
    get_milk_count: вернуть сколько молока можно получить в день,
    get_eggs_count: вернуть сколько яиц можно получить в день.
    """

    def __init__(self, name: str, owner: str):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        return len([i for i in self.zoo_animals if type(i) == Goat])

    def get_chicken_count(self):
        return len([i for i in self.zoo_animals if type(i) == Chicken])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum([i.milk_per_day for i in self.zoo_animals if isinstance(i, Goat)])

    def get_eggs_count(self):
        return sum([i.eggs_per_day for i in self.zoo_animals if isinstance(i, Chicken)])


if __name__ == '__main__':
    my_farm = Farm('Порошкино', 'Вася')
    my_farm.zoo_animals.extend([
        Chicken('Ряба_1', 1, 2),
        Chicken('Ряба_2', 1, 1),
        Goat('Зорька', 3, 5),
        Goat('Маня', 2, 6),
    ])
    print(my_farm.get_goat_count())
    print(my_farm.get_chicken_count())
    print(my_farm.get_animals_count())
    print(my_farm.get_milk_count())
    print(my_farm.get_eggs_count())
