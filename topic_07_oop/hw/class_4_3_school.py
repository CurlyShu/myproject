from topic_07_oop.hw.class_4_1_pupil import Pupil

from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    """
    Класс School.

    Поля:
    список людей в школе (общий list для Pupil и Worker): people,
    номер школы: number.

    Методы:
    get_avg_mark: вернуть средний балл всех учеников школы
    get_avg_salary: вернуть среднюю зп работников школы
    get_worker_count: вернуть сколько всего работников в школе
    get_pupil_count: вернуть сколько всего учеников в школе
    get_pupil_names: вернуть все имена учеников (с повторами, если есть)
    get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
    get_max_pupil_age: вернуть возраст самого старшего ученика
    get_min_worker_salary: вернуть самую маленькую зп работника
    get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
    (список из одного или нескольких элементов)
    """

    def __init__(self, people: list, number: int):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        # all_marks = []
        # all_marks.extend(i.get_all_marks() for i in self.people if isinstance(i, Pupil))
        # return all_marks
        return sum([i.get_avg_mark() for i in self.people if isinstance(i, Pupil)]) / self.get_pupil_count()

    def get_avg_salary(self):
        return sum([i.salary for i in self.people if isinstance(i, Worker)]) / self.get_worker_count()

    def get_worker_count(self):
        return len([i for i in self.people if isinstance(i, Worker)])

    def get_pupil_count(self):
        return len([i for i in self.people if isinstance(i, Pupil)])

    def get_pupil_names(self):
        return [i.name for i in self.people if isinstance(i, Pupil)]

    def get_unique_worker_positions(self):
        return set([i.position for i in self.people if isinstance(i, Worker)])

    def get_max_pupil_age(self):
        return max([i.age for i in self.people if isinstance(i, Pupil)])

    def get_min_worker_salary(self):
        return min([i.salary for i in self.people if isinstance(i, Worker)])

    def get_min_salary_worker_names(self):
        min_salary = self.get_min_worker_salary()
        return [i.name for i in self.people if isinstance(i, Worker) and min_salary == i.salary]


if __name__ == '__main__':
    pupil_1 = Pupil('Oleg', 10, {'math': [5, 3, 4], 'english': [4, 4]})
    pupil_2 = Pupil('Masha', 15, {'math': [5, 5, 5, 3, 3], 'english': [3, 3], 'geography': [4, 5, 5]})
    pupil_3 = Pupil('Misha', 8, {'music': [5, 4], 'painting': [4, 3]})
    worker_1 = Worker('Slava', 50000, 'math teacher')
    worker_2 = Worker('Nadya', 30000, 'chef')
    worker_3 = Worker('Viktor', 48000, 'geography teacher')

    my_school = School([
        pupil_1,
        pupil_2,
        pupil_3,
        worker_1,
        worker_2,
        worker_3
    ], 640)

    print(my_school.get_avg_mark())
    print(my_school.get_avg_salary())
    print(my_school.get_worker_count())
    print(my_school.get_pupil_count())
    print(my_school.get_pupil_names())
    print(my_school.get_unique_worker_positions())
    print(my_school.get_max_pupil_age())
    print(my_school.get_min_worker_salary())
    print(my_school.get_min_salary_worker_names())

