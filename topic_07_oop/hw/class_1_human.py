"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:

    def __init__(self, age, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def get_age(self):
        return self.age

    def __eq__(self, another):
        return self.age == another.age and self.first_name == another.first_name and self.last_name == another.last_name

    def __str__(self):
        return f"Имя: {self.first_name} {self.last_name} Возраст: {self.age}"


if __name__ == '__main__':
    hum_1 = Human(41, 'Olga', 'Nikolaeva')
    hum_2 = Human(41, 'Aleksei', 'Nikolaev')
    print(hum_1)
    print(hum_2)
