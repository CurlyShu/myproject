"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str_1, str_2):
    if len(str_1) < len(str_2):
        return str_1 in str_2
    elif len(str_1) > len(str_2):
        return str_2 in str_1
    else:
        return False
