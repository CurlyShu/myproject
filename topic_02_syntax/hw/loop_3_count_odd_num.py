"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""


# переделать
def count_odd_num(n):
    if type(n) != int:
        return 'Must be int!'
    elif n <= 0:
        return 'Must be > 0!'

    count = 0
    n_str = str(n)
    for i in n_str:
        if int(i) % 2 != 0:
            count += 1

    return count
