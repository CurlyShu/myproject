import json

import telebot
from telebot import types
from telebot.types import Message

from study_project.combat.char_type import CharacterType
from study_project.combat.char_state import CharacterState
from study_project.combat.char_by_type import characters_by_type
from study_project.combat.action import Action
from study_project.combat.game_result import GameResult
from study_project.combat.character import Character
from study_project.combat.character_npc import CharacterNPC

with open('./bot_token.txt', 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}
statistics = {}
stat_file = 'game_stat.json'

action_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                            one_time_keyboard=True,
                                            row_width=len(Action))

action_keyboard.row(*[types.KeyboardButton(action.name) for action in Action])  # уточнить


def update_statistics(chat_id, result: GameResult):
    print('Выполняю обновление статистики')  # уточнить
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.W:
        statistics[chat_id]['W'] = statistics[chat_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[chat_id]['L'] = statistics[chat_id].get('L', 0) + 1
    elif result == GameResult.E:
        statistics[chat_id]['E'] = statistics[chat_id].get('E', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("Статистика обновлена")


def load_statistics():
    print('Выполняю загрузку статистики')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('загрузка завершена')
    except FileNotFoundError:
        statistics = {}
        print('Статистика предыдущих игр не найдена')


@bot.message_handler(commands=['help'])
def help_command(message):
    bot.send_message(message.chat.id,
                     'Привет! Выбери \n/start для начала игры,\n/statistics для отображения статистики')


@bot.message_handler(commands=['stat'])
def stat_command(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = 'Новый пользователь. Статистика пока '
    else:
        user_stat = 'Твои результаты:'
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f'\n{res}: {num}'

    bot.send_message(message.chat.id, text=user_stat)


@bot.message_handler(commands=['start'])
def start_command(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text='Выезжаешь по делам? Разыграем успеешь ли?',
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Отлично, начинаем!')

        create_npc(message)
        ask_user_about_character_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Ок, в другой раз')

    else:
        bot.send_message(message.from_user.id,
                         'Не понимаю тебя')


def create_npc(message):
    print(f'Начало создания объекта NPC для chat id = {message.chat.id}')
    global state
    character_npc = CharacterNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_character'] = character_npc

    npc_image_filename = characters_by_type[character_npc.char_type][character_npc.name]
    bot.send_message(message.chat.id, 'Противник:')
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, character_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_character_type(message):
    markup = types.InlineKeyboardMarkup()

    for character_type in CharacterType:
        markup.add(types.InlineKeyboardButton(text=character_type.name,
                                              callback_data=f'character_type_{character_type.value}'))

    bot.send_message(message.chat.id, "Выбери способ перемещения:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'character_type_' in call.data)
def character_type_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        character_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, 'Выбери кто ты в пробке:')

        ask_user_about_character_by_type(character_type_id, call.message)


def ask_user_about_character_by_type(character_type_id, message):
    character_type = CharacterType(character_type_id)
    character_dict_by_type = characters_by_type.get(character_type, {})

    for character_name, character_img in character_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=character_name,
                                              callback_data=f'character_name_{character_type_id}_{character_name}'))
        with open(f'../images/{character_img}', 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'character_name_' in call.data)
def character_name_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        character_type_id, character_name = int(call_data_split[2]), call_data_split[3]

        create_user_character(call.message, character_type_id, character_name)

        bot.send_message(call.message.chat.id, 'Игра началась!')

        game_next_step(call.message)


def create_user_character(message, character_type_id, character_name):
    print(f'Начало создания объекта Character для chat id = {message.chat.id}')
    global state
    user_character = Character(name=character_name,
                               char_type=CharacterType(character_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_character'] = user_character

    image_filename = characters_by_type[user_character.char_type][user_character.name]
    bot.send_message(message.chat.id, "Твой выбор:")
    with open(f'../images/{image_filename}', 'rb') as file:
        bot.send_photo(message.chat.id, file, user_character)

    print(f'Завершено создание объекта Character для chat id = {message.chat.id}')


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     'Избежать действия соседа по пробке:',
                     reply_markup=action_keyboard)
    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not Action.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Твои действия:",
                         reply_markup=action_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_action=message.text)


def reply_attack(message: Message, defend_action: str):
    if not Action.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_action = message.text

        global state
        user_character = state[message.chat.id]['user_character']
        npc_character = state[message.chat.id]['npc_character']

        user_character.next_step_points(next_attack=Action[attack_action],
                                        next_defence=Action[defend_action])

        npc_character.next_step_points()

        game_step(message, user_character, npc_character)


def game_step(message: Message, user_character: Character, npc_character: Character):
    comment_npc = npc_character.get_hit(opponent_attack_point=user_character.attack_point,
                                        opponent_hit_power=user_character.delay,
                                        opponent_type=user_character.char_type)
    bot.send_message(message.chat.id, f'Сосед по пробке:  {comment_npc}\nВремя опоздания: {npc_character.time}')

    comment_user = user_character.get_hit(opponent_attack_point=npc_character.attack_point,
                                          opponent_hit_power=npc_character.delay,
                                          opponent_type=npc_character.char_type)
    bot.send_message(message.chat.id, f'Ты:  {comment_user}\nВремя опоздания: {user_character.time}')

    if npc_character.state == CharacterState.IN_TRAFFIC and user_character.state == CharacterState.IN_TRAFFIC:
        bot.send_message(message.chat.id, 'Продолжаем толкаться в пробке')
        game_next_step(message)
    elif npc_character.state == CharacterState.LATE and user_character.state == CharacterState.LATE:
        bot.send_message(message.chat.id, 'Опоздали все! За что нам это?')
        update_statistics(message.chat.id, GameResult.E)
    elif npc_character.state == CharacterState.LATE:
        bot.send_message(message.chat.id, 'Я полетел! Чао!')
        update_statistics(message.chat.id, GameResult.W)
    elif npc_character.state == CharacterState.LATE:
        bot.send_message(message.chat.id, 'Ну встреться мне еще на дороге!')
        update_statistics(message.chat.id, GameResult.L)


if __name__ == '__main__':
    load_statistics()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
