from enum import Enum, auto


class CharacterState(Enum):
    IN_TRAFFIC = auto()
    LATE = auto()
