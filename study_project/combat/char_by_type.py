from study_project.combat.char_type import CharacterType


characters_by_type = {
    CharacterType.WALKER: {'Пешеход': 'Walker.jpeg'},

    CharacterType.CAR_DRIVER: {'Car driver 1': 'Car driver 1.jpg',
                               'Car driver 2': 'Car driver 2.jpg'},

    CharacterType.CYCLIST: {'Cyclist 1': 'Cyclist 1.jpg',
                            'Cyclist 2': 'Cyclist 2.jpg',
                            'Cyclist 3': 'Cyclist 3.jpg'},

    CharacterType.SCOOTERIST: {'Scooterist 1': 'Scooterist 1.jpg',
                               'Scooterist 2': 'Scooterist 2.jpg',
                               'Scooterist 3': 'Scooterist 3.jpg'}
}
