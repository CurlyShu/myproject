from study_project.combat.char_type import CharacterType

char_weaknesses = {
    CharacterType.WALKER: (CharacterType.CYCLIST, CharacterType.SCOOTERIST),
    CharacterType.CAR_DRIVER: (CharacterType.WALKER, CharacterType.CYCLIST),
    CharacterType.CYCLIST: (CharacterType.SCOOTERIST, ),
    CharacterType.SCOOTERIST: (CharacterType.CAR_DRIVER, )
}
