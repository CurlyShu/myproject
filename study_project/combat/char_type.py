from enum import Enum, auto


class CharacterType(Enum):
    WALKER = auto()
    CAR_DRIVER = auto()
    CYCLIST = auto()
    SCOOTERIST = auto()

    @classmethod
    def min_value(cls):
        return cls.WALKER.value

    @classmethod
    def max_value(cls):
        return cls.SCOOTERIST.value
