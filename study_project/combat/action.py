from enum import Enum, auto


class Action(Enum):
    nothing = auto()
    honk_the_horn = auto()
    cut_off_on_the_road = auto()
    quarrel = auto()

    @classmethod
    def min_val(cls):
        return cls.nothing.value

    @classmethod
    def max_val(cls):
        return cls.quarrel.value

    @classmethod
    def has_item(cls, name):
        return name in cls._member_names_   # уточнить
