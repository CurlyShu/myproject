from study_project.combat.char_type import CharacterType
from study_project.combat.char_types_weaknesses import char_weaknesses
from study_project.combat.char_state import CharacterState
from study_project.combat.action import Action


class Character:
    def __init__(self, name, char_type: CharacterType):
        self.name = name
        self.char_type = char_type
        self.weaknesses = char_weaknesses.get(char_type, tuple())
        self.time = 0
        self.defence_point = None
        self.attack_point = None
        self.delay = 10
        self.state = CharacterState.IN_TRAFFIC

    def __str__(self):
        return f"Name: {self.name} | Type: {self.char_type.name} | Delay time : {self.time} minutes"

    def next_step_points(self, next_attack: Action, next_defence: Action):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: Action,
                opponent_hit_power: int,
                opponent_type: CharacterType):
        if opponent_attack_point == Action.nothing:
            return "Хорошего дня!"
        elif self.defence_point == opponent_attack_point:
            return "Посторонись"
        else:
            self.time += opponent_hit_power * (1.5 if opponent_type in self.weaknesses else 1)

            if self.time >= 60:
                self.state = CharacterState.LATE
                return "Конкретно опоздал"
            else:
                return "Смотри куда едешь, слепошара!"
