import random
from study_project.combat.character import Character
from study_project.combat.char_type import CharacterType
from study_project.combat.char_by_type import characters_by_type
from study_project.combat.action import Action


class CharacterNPC(Character):
    def __init__(self):
        rand_char_value = random.randint(CharacterType.min_value(), CharacterType.max_value())
        rand_char_type = CharacterType(rand_char_value)
        rand_char_name = random.choice(list(characters_by_type.get(rand_char_type, {}).keys()))

        super().__init__(rand_char_name, rand_char_type)

    def next_step_points(self, **kwargs):
        attack_points = Action(random.randint(Action.min_val(), Action.max_val()))
        defence_points = Action(random.randint(Action.min_val(), Action.max_val()))
        super().next_step_points(next_attack=attack_points, next_defence=defence_points)


if __name__ == '__main__':
    npc_character = CharacterNPC()
    npc_character.next_step_points()
    print(npc_character)