"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if not my_dict:
        return [], [], 0, 0

    list_keys = list(my_dict.keys())
    list_values = list(my_dict.values())
    set_values = set(my_dict.values())
    return list_keys, list_values, len(my_dict.keys()), len(set_values)


if __name__ == '__main__':
    ex_dict = {1: 'w', 2: 'w', 3: 'w'}
    print(dict_to_list(ex_dict))
